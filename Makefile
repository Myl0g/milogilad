all: profile blog

.PHONY: all

submodules:
	@git submodule init
	@git submodule update --remote --merge

profile: submodules
	$(MAKE) -C ./src/profile
	@mv src/profile/public ./public

blog: submodules
	$(MAKE) -C ./src/blog

clean:
	@rm -rf ./public
	$(MAKE) -C ./src/blog clean

serve: all
	(cd ./public && python3 -m http.server)

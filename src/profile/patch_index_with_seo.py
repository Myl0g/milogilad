seo = b'''
<script type="application/ld+json">
{
    "@context": "https://schema.org/",
    "@type": "Person",
    "name": "Milo Jonathan Gilad",
    "url": "https://milogilad.com",
    "image": "https://milogilad.com/images/avatar.jpg",
    "sameAs": "https://linkedin.com/in/milogilad",
    "jobTitle": "Chief Technology Officer",
    "worksFor": {
    "@type": "Corporation",
    "name": "NewGrade"
    }
}
</script>
'''

import subprocess
import shutil
import os

os.chdir('public')

lines = []
with open('index.html', 'rb') as f:
    lines = f.readlines()

with open('index.html', 'wb') as f:
    i = 0
    for line in lines:
        f.writelines([line])
        i += 1
        if i == 4:
            f.write(seo)
